/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  API_ISSUE_SAVEFEEDBACK,
  API_ISSUE_SENDCODE,
  API_PlatformCache_CACHES,
  API_PlatformCache_CLEARALL,
  API_PlatformDevelop_GETALLDATA,
  API_PlatformDevelop_SAVECONFIGDATA,
  API_PlatformManage_ADDSCHEME,
  API_PlatformManage_DELETESCHEME,
  API_PlatformManage_EDITSCHEME,
  API_PlatformManage_GETALLPRODUCT,
  API_PlatformManage_GETALLSCHEME,
  API_PlatformManage_GETSCHEME,
  API_PlatformManage_SENDEEMAIL,
  API_COMMON_DICTION,
  API_DICTION_LOADTREE,
  API_LOG_GETTREE_ITEM,
  API_LOG_UNREGIST,
  API_LOG_BUTTON_STATUS,
} from './urls';

/**
 * 获取控制台日志开启状态
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getConsoleItemStatus(params, headers) {
  return ajax({ url: API_LOG_BUTTON_STATUS, params, headers }).then((info) => {
    return info;
  });
}
/**
 * 获取控制台日志的树形列表
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function unRegistConsoleItem(params, headers) {
  return ajax({ url: API_LOG_UNREGIST, params, headers }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取控制台日志的树形列表
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getConsoleTreeItem(params, headers) {
  return ajax({ url: API_LOG_GETTREE_ITEM, params, headers }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取系统设置管理,调用获得全部回显数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getAllData(params) {
  return ajax({ url: API_PlatformDevelop_GETALLDATA, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 修改保存接口
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveConfigData(params) {
  return ajax({ url: API_PlatformDevelop_SAVECONFIGDATA, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查看所有方案接口
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getAllScheme(params) {
  return ajax({ url: API_PlatformManage_GETALLSCHEME, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查看某个方案信息
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getScheme(params) {
  return ajax({ url: API_PlatformManage_GETSCHEME, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 新添方案
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function addScheme(params) {
  return ajax({ url: API_PlatformManage_ADDSCHEME, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 删除某个方案
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function deleteScheme(params) {
  return ajax({ url: API_PlatformManage_DELETESCHEME, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 修改保存方案
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function editScheme(params) {
  return ajax({ url: API_PlatformManage_EDITSCHEME, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 所有可使用产品
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getAllProduct(params) {
  return ajax({ url: API_PlatformManage_GETALLPRODUCT, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 调试发送邮件
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function sendeMail(params) {
  return ajax({ url: API_PlatformManage_SENDEEMAIL, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

//#region 缓存相关
/**
 * 获取所有缓存数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getCachesApi(params) {
  return ajax({ method: 'GET', url: API_PlatformCache_CACHES, params }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取所有缓存数据
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function clearAllCachesApi(params, headers) {
  return ajax({
    method: 'GET',
    url: API_PlatformCache_CLEARALL,
    params,
    headers,
  }).then((info) => {
    if (info.success) {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

//#endregion

//#region 问题反馈
/**
 * 短信验证码
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function sendCodeApi(params) {
  return ajax({ url: API_ISSUE_SENDCODE, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 反馈信息
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function saveFeedbackApi(params) {
  return ajax({ url: API_ISSUE_SAVEFEEDBACK, params }).then((info) => {
    if (info.success) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获取字典数据 API_COMMON_DICTION
 * @param {Object} params
 * @return {Promise}
 */
export function getDicItemByCodes(params) {
  return ajax({ url: API_COMMON_DICTION, params }).then((info) => {
    if (info) {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 根据字典ID或取字典树
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getDictionaryTree(params) {
  return ajax({ url: API_DICTION_LOADTREE, params: params }).then((info) => {
    return info;
  });
}
//#endregion
