/**
 * API_URL命名规则：API_模块_方法
 */
// 获取系统设置管理,调用获得全部回显数据
export const API_PlatformDevelop_GETALLDATA = '/je/meta/setting/selectSystemConfig';

// 修改保存接口
export const API_PlatformDevelop_SAVECONFIGDATA = '/je/meta/setting/writeSysVariables';

// 查看所有方案
export const API_PlatformManage_GETALLSCHEME = '/je/meta/settingPlan/loadSettingPlans';

// 查看某个方案信息
export const API_PlatformManage_GETSCHEME = '/je/meta/settingPlan/loadSettingPlanInfo';

// 新添方案
export const API_PlatformManage_ADDSCHEME = '/je/meta/settingPlan/insertSettingPlan';

// 修改保存方案
export const API_PlatformManage_EDITSCHEME = '/je/meta/settingPlan/saveSettingPlan';

// 删除方案
export const API_PlatformManage_DELETESCHEME = '/je/meta/settingPlan/deleteSettingPlan';

// 所有可使用产品
export const API_PlatformManage_GETALLPRODUCT = '/je/meta/product/load';

// 调试发送邮件
export const API_PlatformManage_SENDEEMAIL = '/je/meta/setting/sendeEmail';

//#region
// 获取所有缓存数据
export const API_PlatformCache_CACHES = '/je/meta/cache/caches';

// 清除缓存
export const API_PlatformCache_CLEARALL = '/je/common/cache/manage/clearAll';

/* 问题反馈 */
// 发送验证码
export const API_ISSUE_SENDCODE = '/je/meta/questionFeedback/sendCode';

// 信息保存
export const API_ISSUE_SAVEFEEDBACK = '/je/meta/questionFeedback/save';

//获取字典数据
export const API_COMMON_DICTION = '/je/meta/dictionary/getDicItemByCodes';

//获取字典树结构数据
export const API_DICTION_LOADTREE = '/je/meta/dictionary/tree/loadTree';

// 获取控制日志的tree的列表
export const API_LOG_GETTREE_LIST = '/je/meta/product/plan/manage/load';
// 获取控制日志的item的数据
export const API_LOG_GETTREE_ITEM = '/je/message/log/realtime/regist';
// 取消控制日志的item的数据
export const API_LOG_UNREGIST = '/je/message/log/realtime/unregist';
//获取控制日志服务务的显示停止状态
export const API_LOG_BUTTON_STATUS = '/je/message/log/realtime/status';
//#endregion
