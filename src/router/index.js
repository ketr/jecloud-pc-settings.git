import baseRoutes from '@common/router/routes';
import PlatformDevelop from '@/views/platform-develop/index.vue';
import PlatformManage from '@/views/platform-manage/index.vue';
import PlatformCache from '@/views/platform-cache/index.vue';
import PlatformService from '@/views/platform-service/index.vue';
import PlatformConsole from '@/views/platform-console/index.vue';
import Issue from '@/views/issues/index.vue';
import { t } from '@/locales';

/**
 * 基础路由
 * 如果有业务需要，可以自行调整
 * 固定路由：Home(/),Login(/login)
 */
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/develop',
  },
  {
    path: '/develop',
    text: t('developOption'),
    menu: true,
    name: 'PlatformDevelop',
    component: PlatformDevelop,
  },
  {
    path: '/system',
    text: t('system'),
    menu: true,
    name: 'PlatformSystem',
    component: PlatformManage,
  },
  {
    path: '/cache',
    text: t('cache'),
    menu: true,
    name: 'PlatformCache',
    component: PlatformCache,
  },
  {
    path: '/issue',
    text: t('issue'),
    menu: true,
    name: 'Issue',
    component: Issue,
  },
  {
    path: '/service',
    text: t('service'),
    menu: true,
    name: 'PlatformService',
    component: PlatformService,
  },
  {
    path: '/console',
    text: t('console'),
    menu: true,
    name: 'PlatformConsole',
    component: PlatformConsole,
  },
  ...baseRoutes,
];
export default routes;

/**
 * 自定义路由History
 * @returns History
 */
// export function createRouterHistory() {
//   return createMemoryHistory();
// }

/**
 * 自定义路由守卫
 */
// export function createRouterGuard(router) {
//   // 业务逻辑
// }

/**
 * 路由白名单
 */
//  export const whiteRoutes = [];
